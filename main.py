#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os.path
import sys
import json

try:
    import apiai
except ImportError:
    sys.path.append(
        os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir)
    )
    import apiai

CLIENT_ACCESS_TOKEN = '7f109c1f355c4e62815409938d71594f'


def main():
    print("--- Start Communication with API AI agent ---")
    print("--- Type 'X' at any point in time to terminate conversation ---")
    ai = apiai.ApiAI(CLIENT_ACCESS_TOKEN)


    while True:

        userMsg = input(">")

        if (userMsg == "X"):
            break
        else:
            request = ai.text_request()
            request.lang = 'en'  # optional, default value equal 'en'

            # request.session_id = "<SESSION ID, UNIQUE FOR EACH USER>"

            request.query = userMsg

            # response = request.getresponse().read()
            # textResponse = response['result']['fulfillment']['speech'].read();

            response = json.loads(request.getresponse().read().decode('utf-8'))
            responseStatus = response['status']['code']
            if (responseStatus == 200):
                print(u"< %s" % response['result']['fulfillment']['speech'])

                result = response['result']
                action = result.get('action')

                if action is not None:
                    if action == u"send_message":
                        print("(Debug): Searching Wikipedia")
                        # Do something
            else:
                print ("< Sorry, I couldn't understand that question")



if __name__ == '__main__':
    main()